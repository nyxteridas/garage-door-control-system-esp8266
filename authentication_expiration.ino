//#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <time.h>

const char* ssid = "YOURSSID"; //type your ssid
const char* password = "YOURPASSWORD"; //type your password

int oude = 12; // phase current (pin connected to relay)
int fasi = 13; // neutral current (pin connected to relay)

//This bool is used to control device lockout 
bool lock = false; 

//anchars will be explained below. username will be compared with 'user' and loginPassword with 'pass' when login is done
String anchars = "abcdefghijklmnopqrstuvwxyz0123456789", username = "USERNAME_FOR_WEB", loginPassword = "PASSWORD_FOR_WEB"; 

//First 2 timers are for lockout and last one is inactivity timer
unsigned long logincld = millis(), reqmillis = millis(), tempign = millis(); 

// i is used for for index, trycount will be our buffer for remembering how many false entries there were
uint8_t i, trycount = 0; 

//this is cookie buffer
String sessioncookie; 

//static IP setup
IPAddress ip(192, 168, 1, 18);
IPAddress gateway( 192, 168, 1, 1 );
IPAddress subnet( 255, 255, 255, 0 ); 

ESP8266WebServer server(80);

//setting up the module
void setup(void) {

  Serial.begin(115200);
  delay(10);
  pinMode(oude, OUTPUT);
  pinMode(fasi, OUTPUT);
  digitalWrite(oude, LOW);
  digitalWrite(fasi, LOW);

  delay(10);
  // Connect to WiFi network
  Serial.println();   
  Serial.print("Connecting to ");
  Serial.println(ssid); 
  WiFi.config(ip, gateway, subnet);  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
   
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
   
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

  //generate new cookie on device start
  gencookie(); 
  
  server.on("/", handleRoot);
  server.on("/login", handleLogin);
  server.on("/logoff", logoff);

  server.onNotFound(handleNotFound);
  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent", "Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys) / sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize);
  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {  
  server.handleClient();
  
    if(lock && abs(millis() - logincld) > 300000){
      lock = false;
      trycount = 0;
      //After 5 minutes is passed unlock the system
      logincld = millis(); 
    }
    
    if(!lock && abs(millis() - logincld) > 60000){
      trycount = 0;
      logincld = millis();
      //After minute is passed without bad entries, reset trycount
    }
   
   if(abs(millis() - tempign) > 120000){
     gencookie();
     tempign = millis();
     //if there is no activity from logged on user, change the generate a new cookie. This is more secure than adding expiry to the cookie header
  } 
}

//This function checks for Cookie header in request, it compares variable c to sessioncookie and returns true if they are the same
bool is_authentified() {
  if (server.hasHeader("Cookie")) {
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie"), authk = "c=" + sessioncookie;
    Serial.println(cookie);
    if (cookie.indexOf(authk) != -1) return true;
  }
  return false;
}

//login page, also called for disconnect
void handleLogin() {

  String msg;
  if (server.hasHeader("Cookie")) {
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie");
    Serial.println(cookie);
  }
  
  //if user posted with these arguments
  if (server.hasArg("USERNAME") && server.hasArg("PASSWORD")){ 
    //check if login details are good and dont allow it if device is in lockdown
    if (server.arg("USERNAME") == username &&  server.arg("PASSWORD") == loginPassword && !lock){ 
      //if above values are good, send 'Cookie' header with variable c, with format 'c=sessioncookie'
      server.sendHeader("Location", "/");
      server.sendHeader("Cache-Control", "no-cache");
      server.sendHeader("Set-Cookie", "c=" + sessioncookie);
      server.send(301);
      Serial.println("Log in Successful");
      //With good headers in mind, reset the trycount buffer
      trycount = 0; 
      return;
    }
    Serial.println("Log in Failed");
  
    //If system is not locked up the trycount buffer
    if (trycount != 10 && !lock)
      trycount++; 
    
    //We go here if systems isn't locked out, we give user 10 times to make a mistake after we lock down the system, thus making brute force attack almost imposible    
    if (trycount < 10 && !lock){ 
      msg += "Wrong username/password, try again.</br>";
      msg += "You have ";
      msg += (10 - trycount);
      msg += " tries left.";
      //Reset the logincld timer, since we still have available tries
      logincld = millis(); 
    }
  
    //If too much bad tries
    if (trycount == 10){ 
      if(lock){
        msg += "Your account will remain locked, due to several failed login attempts, for ";
        //Display lock time remaining in minutes
        msg += 5 - ((millis() - logincld) / 60000); 
        msg += " minutes.";
        Serial.println("Locked out!");
      }
      else{
         logincld = millis();
         lock = true;  
         //This happens when your device first locks down
         msg += "You can use again your device in 5 minutes."; 
         Serial.println("Locked out!");
      }   
    }
  }

  if (server.hasArg("LED")) {
    if(is_authentified()){
      if(server.arg("LED") == "ON"){
        Serial.println("GARAGE OPEN");
        digitalWrite(oude, HIGH);
        digitalWrite(fasi, HIGH);
        server.sendHeader("Location", "/");
        server.sendHeader("Cache-Control", "no-cache");
        server.sendHeader("Set-Cookie", "c=" + sessioncookie);
        server.send(301);
      }
      else if (server.arg("LED") == "OFF"){
        Serial.println("GARAGE CLOSE");
        digitalWrite(oude, LOW);
        digitalWrite(fasi, LOW);
        server.sendHeader("Location", "/");
        server.sendHeader("Cache-Control", "no-cache");
        server.sendHeader("Set-Cookie", "c=" + sessioncookie);
        server.send(301);
      }
    }
    return;
  }
  
  String content = "<!DOCTYPE HTML><html><head><meta name=\"viewport\" content=\"width=device-width,height=device-height,initial-scale=1.0\" /><meta charset=\"utf-8\"><title>Garage Control</title>";
  content += "<link rel=\"stylesheet prefetch\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">";
  content += "<style>.modal-content{background-color: darkcyan;} .btn-link{color:white;} .modal-heading h2{color:#ffffff;}</style></head>";
  content += "<body style=\"background-color:#e5e5e5;\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-heading\"><h2 class=\"text-center\">Σύνδεση</h2>";
  content += "</div><hr /><div class=\"modal-body\"><form action='/login' method='POST' role=\"form\"><div class=\"form-group\"><div class=\"input-group\">";
  content += "<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>";
  content += "<input type=\"text\" name='USERNAME' class=\"form-control\" placeholder=\"Username\" /></div></div>";
  content += "<div class=\"form-group\"><div class=\"input-group\"><span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-lock\"></span></span>";
  content += "<input type=\"password\" name='PASSWORD' class=\"form-control\" placeholder=\"Password\" /></div></div>";
  content += "<span style='color:#8b0000;'><b><i>" + msg + "</i></b></span>";
  content += "<div class=\"form-group text-center\"><input type=\"submit\" name='SUBMIT' class=\"btn btn-success btn-lg\" style=\"border:#008b00; background-color:#008b00;\" value='Login'>";  
  content += "</div></form></div>";
  content += "</div></div></body></html>";
  server.send(200, "text/html", content);
}

void gencookie(){
  sessioncookie = "";
  for( i = 0; i < 32; i++) sessioncookie += anchars[random(0, anchars.length())]; //Using randomchar from anchars string generate 32-bit cookie
}

//root page can be accessed only if authentification is ok
void handleRoot() {
  Serial.println("Enter handleRoot");
  String header;
  //This here checks if your cookie is valid in header and it redirects you to /login if not
  if (!is_authentified()) { 
    server.sendHeader("Location", "/login");
    server.sendHeader("Cache-Control", "no-cache");
    server.send(301);
    return;
  }
  
  //reset the inactivity timer if someone logs in
  tempign = millis(); 

  //read current state of power
  int value = digitalRead(oude);
  
  String content = "<!DOCTYPE HTML><html><head><meta name=\"viewport\" content=\"width=device-width,height=device-height,initial-scale=1.0\" /><meta charset=\"utf-8\"><title>Garage Control</title>";
  content += "<link rel=\"stylesheet prefetch\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">";
  content += "<style>.modal-content{background-color: darkcyan;} .btn-link{color:white;} .modal-heading h2{color:#ffffff;}";
  content += ".modal-body h4 a:link, a:visited a:hover, a:active{color:#8b0000;}</style></head><body style=\"background-color:#e5e5e5;\">>";
  content += "<div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-heading\"><h2 class=\"text-center\">System Status</h2></div><hr />";
  content += "<div class=\"modal-body\"><div class=\"form-group text-center\">";

  if(value == HIGH) {
    content += "<a href=\"/login?LED=OFF\" class=\"btn btn-success btn-lg\" style=\"border:#008b00; background-color:#008b00;\">Enabled</a>";
  } else {
    content += "<a href=\"/login?LED=ON\" class=\"btn btn-success btn-lg\" style=\"border:#8b0000; background-color:#8b0000;\">Disabled</a>";
  }

  content += "</div></div><hr />";
  content += "<div class=\"modal-body\"><div class=\"form-group text-center\"><h4>Don't forget to <a href=\"/logoff\">logout</a></h4></div></div>";
  content += "</div></div></body></html>";
  server.send(200, "text/html", content);
}

//no need authentification
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void logoff(){
  Serial.println("Disconnection");
  server.sendHeader("Location", "/login");
  server.sendHeader("Cache-Control", "no-cache");
  //Set 'c=0', it users header, effectively deleting it's header
  server.sendHeader("Set-Cookie", "c=0");
  server.send(301);
  return;
}
