## Garage Door Control System v1.0
**Written for ESP8266**

Via internet you can access the device, and with a pair of username/password you are able to open or close door's power supply.
The construction was made by Panagiotis Loukouzas, and the programming by both of us.

In the repository you can find also the windows driver for ESP8266

---

## Parameters which need to be set according to your needs

1. ssid (in order to connect through wifi)
2. password (network's password)
3. ip (IP address to be used)
4. gateway (Gateway's IP)
5. subnet
6. oude (phase current - pin connected to relay)
7. fasi (neutral current - pin connected to relay)
8. username (username for website)
9. loginPassword (password for website)
10. server (listening port of web server)

---

## Which endpoints are supported

The endpoints that are supported are:

1. /
2. /login
3. /logoff

In case the endpoint is not one of the above, 404 not found is returned and handled by the controller.

---

## Lockout from the device

In case a user inserts 10 times wrong credentials, the account is locked for 5 minutes.

---

## Cookies support

This implementation also sets a cookie to user's browser in order to handle more effectively the session.
The cookie is deleted upon logoff.

---

## How it works

After you have logged in the system, you can see a screen which indicates if the power supply is on (green) or off (red).
Clicking on the indicator, the power supply will turn on if it was off, and vice-versa.